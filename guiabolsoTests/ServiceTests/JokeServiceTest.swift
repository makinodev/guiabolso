//
//  JokeServiceTest.swift
//  guiabolsoTests
//
//  Created by Matheus Goveia on 13/09/18.
//  Copyright © 2018 MAOG. All rights reserved.
//

import Foundation
import XCTest
import Nimble
@testable import guiabolso

class JokeServiceTest: XCTestCase {

  func testGetRandomJoke() {
    
    // given
    let category = "Dev"
    var joke: Joke? = nil

    // when
    JokeService.getRandomJoke(fromCategory: category) { (results) in
      results.handle(
        successful: { (result) in
          joke = result
      })
    }

    // then
    expect(joke).toNotEventually(beNil(), timeout: 5.0)
  }
}
