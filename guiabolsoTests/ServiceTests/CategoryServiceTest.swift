//
//  CategoryServiceTest.swift
//  guiabolsoTests
//
//  Created by Matheus Goveia on 13/09/18.
//  Copyright © 2018 MAOG. All rights reserved.
//

import Foundation
import XCTest
import Nimble
@testable import guiabolso

class CategoryServiceTest: XCTestCase {
  
  func testGetAllCategories() {
    
    // given
    var categories: Result<[guiabolso.Category]>? = nil

    // when
    CategoryService.getAllCategories { (result) in
      categories = result
    }

    // then
    expect(categories).toNotEventually(beNil(), timeout: 5.0)
    expect(categories?.isSuccess).to(beTrue())
  }

}
