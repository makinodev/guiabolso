//
//  RandomJokePresenterTest.swift
//  guiabolsoTests
//
//  Created by Matheus Goveia on 13/09/18.
//  Copyright © 2018 MAOG. All rights reserved.
//

import Foundation
import Nimble
import XCTest
@testable import guiabolso

class RandomJokePresenterTest: XCTestCase {

  func testGetRandomJoke() {

    // given
    let presenter = RandomJokePresenter()
    let randomJokeView = RandomJokeViewTest()

    // when
    presenter.setCategory(category: "Dev")
    presenter.attach(view: randomJokeView)
    presenter.getRandomJoke()

    // then
    expect(randomJokeView.joke).toNotEventually(beNil(), timeout: 5.0)
  }
}


fileprivate class RandomJokeViewTest: RandomJokeView {

  var joke: Joke? = nil

  func showJoke(joke: Joke) {
    self.joke = joke
  }

  func showAlert(withTitle title: String, andMessage message: String) { }
  func showLoading() { }
  func hideLoading() { }
}
