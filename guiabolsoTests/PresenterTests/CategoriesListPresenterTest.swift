//
//  CategoriesListPresenterTest.swift
//  guiabolsoTests
//
//  Created by Matheus Goveia on 13/09/18.
//  Copyright © 2018 MAOG. All rights reserved.
//

import Foundation
import Nimble
import XCTest
@testable import guiabolso

class CategoriesListPresenterTest: XCTestCase {

  func testGetCategories() {

    // given
    let presenter = CategoriesListPresenter()
    let view = CategoriesListViewTest()

    // when
    presenter.attach(view: view)
    presenter.getAllCategories()

    // then
    expect(view.categories).toNotEventually(haveCount(0), timeout: 5.0)
  }
}


fileprivate class CategoriesListViewTest: CategoriesListView {

  fileprivate var categories = [guiabolso.Category]()

  func updateTable(withDataSource dataSource: [guiabolso.Category]) {
    self.categories = dataSource
  }

  func showAlert(withTitle title: String, andMessage message: String) { }
  func showLoading() { }
  func hideLoading() { }
}
