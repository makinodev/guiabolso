//
//  RandomJokeTest.swift
//  guiabolsoTests
//
//  Created by Matheus Goveia on 16/09/18.
//  Copyright © 2018 MAOG. All rights reserved.
//

import Foundation
import Nimble
@testable import guiabolso

class RandomJokeTest: KIFTestCase {

  override func afterEach() {
    self.goBackToCategoriesList()
  }

  func testGetRandomJokeFromRandomCategory() {

    self.waitForLogoScreenToDissappear()

    if let tableview = self.getTableView() as? UITableView {

      let numberOfRows = tableview.numberOfRows(inSection: tableview.numberOfSections - 1)
      let randomNumber = arc4random_uniform(UInt32(numberOfRows))

      self.tapRandomRow(random: randomNumber, fromTableView: tableview)

      self.waitForLoadingScreenToDissappear()
      self.checkChuckNorrisIconExists()
    }
  }
}

extension KIFTestCase {

  func goBackToCategoriesList() {
    tester().tapView(withAccessibilityLabel: "Categories")
  }

  func getCell(fromRow row: UInt32, fromTableView tableView: UITableView) -> UITableViewCell {
    let indexPath = IndexPath(row: Int(row), section: 0)
    return tester().waitForCell(at: indexPath, in: tableView)
  }

  func checkChuckNorrisIconExists() {
    tester().waitForView(withAccessibilityLabel: AccessibilityLabels.iconImageView)
  }

  func waitForLoadingScreenToDissappear() {
    tester().waitForView(withAccessibilityLabel: AccessibilityLabels.loadingView)
  }

  func tapRandomRow(random: UInt32, fromTableView tableView: UITableView) {
    let indexPath = IndexPath(row: Int(random), section: 0)
    tester().tapRow(at: indexPath, in: tableView)
  }
}
