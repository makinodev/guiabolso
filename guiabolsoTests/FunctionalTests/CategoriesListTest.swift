//
//  CategoriesListTest.swift
//  guiabolsoTests
//
//  Created by Matheus Goveia on 16/09/18.
//  Copyright © 2018 MAOG. All rights reserved.
//

import Foundation
import Nimble

@testable import guiabolso

class CategoriesListTest: KIFTestCase {

  func testNumberOfCategories() {

    self.waitForLogoScreenToDissappear()

    if let tableview = self.getTableView() as? UITableView {

      let numberOfRows = tableview.numberOfRows(inSection: tableview.numberOfSections - 1)
      expect(numberOfRows).to(beGreaterThan(0))

    }
  }

  func testRefreshTableView() {
    self.waitForLogoScreenToDissappear()
    self.pullDownTableView()
    self.waitForLoadingScreenToDissappear()
  }
  
}

extension KIFTestCase {

  func pullDownTableView() {
    tester().pullToRefreshView(withAccessibilityLabel: AccessibilityLabels.categoriesListTableView, pullDownDuration: KIFPullToRefreshTiming.inAboutOneSecond)
  }

  func getTableView() -> UIView? {
    return tester().waitForView(withAccessibilityLabel: AccessibilityLabels.categoriesListTableView)
  }

  func waitForLogoScreenToDissappear() {
    tester().waitForAbsenceOfView(withAccessibilityLabel: AccessibilityLabels.rootView)
  }
}
