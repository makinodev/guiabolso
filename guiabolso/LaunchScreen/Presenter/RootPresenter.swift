//
//  RootPresenter.swift
//  guiabolso
//
//  Created by Matheus Goveia on 14/09/18.
//  Copyright © 2018 MAOG. All rights reserved.
//

import Foundation

protocol RootView: class {
  func showNextScreen(withData data: [Category])
  func retryTimer(forSeconds seconds: Int)
}

class RootPresenter {

  private weak var view: RootView? = nil
  
  init() { }

  func attach(view: RootView?) {
    self.view = view
  }

  func detachView() {
    self.view = nil
  }

  func getAllCategories() {
    CategoryService.getAllCategories { (results) in
      results.handle(
        successful: { (categories) in
          self.view?.showNextScreen(withData: categories)
      },
        failure: { (error) in
          let seconds: Int = 6
          self.view?.retryTimer(forSeconds: seconds)
      })
    }
  }

}
