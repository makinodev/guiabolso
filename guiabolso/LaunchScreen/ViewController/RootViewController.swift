//
//  LaunchScreenViewController.swift
//  guiabolso
//
//  Created by Matheus Goveia on 13/09/18.
//  Copyright © 2018 MAOG. All rights reserved.
//

import Foundation
import UIKit

class RootViewController: UIViewController {

  @IBOutlet private weak var feedbackLabel: UILabel? {
    didSet {
      self.feedbackLabel?.textColor = Colors.title
      self.feedbackLabel?.text = "Loading"
      self.feedbackLabel?.adjustFontSize()
    }
  }

  @IBOutlet private weak var logoImageView: UIImageView? {
    didSet {
      self.logoImageView?.image = #imageLiteral(resourceName: "chucknorris_logo_coloured_small")
    }
  }

  let presenter = RootPresenter()
  var timer: Timer? = nil
  var seconds: Int = 0

  override func viewDidLoad() {
    self.setupViewController()
    self.setupPresenter()
  }

  func setupPresenter() {
    self.presenter.attach(view: self)
    self.presenter.getAllCategories()
  }

  func setupViewController() {
    self.view.backgroundColor = Colors.main
    self.view.accessibilityLabel = AccessibilityLabels.rootView
  }

  override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
    if
      segue.identifier == Segues.rootToCategoriesList,
      let destination = (segue.destination as? UINavigationController)?.topViewController as? CategoriesListViewController,
      let data = sender as? [Category] {
      destination.updateTable(withDataSource: data)
    }
  }

  func startTimer() {
    self.timer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(RootViewController.timerMethod(timer:)), userInfo: nil, repeats: true)
  }

  @objc func timerMethod(timer: Timer?) {
    if (seconds <= 1) {
      self.feedbackLabel?.text = "Loading"
      timer?.invalidate()
      self.presenter.getAllCategories()
    } else {
      self.feedbackLabel?.text = "Retrying in \(seconds - 1)"
      self.seconds -= 1
    }
  }

}

extension RootViewController: RootView {

  func showNextScreen(withData data: [Category]) {
    self.performSegue(withIdentifier: Segues.rootToCategoriesList, sender: data)
  }

  func retryTimer(forSeconds seconds: Int) {
    self.seconds = seconds
    self.startTimer()
  }

}
