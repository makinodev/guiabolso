//
//  ViewController.swift
//  guiabolso
//
//  Created by Matheus Goveia on 13/09/18.
//  Copyright © 2018 MAOG. All rights reserved.
//

import UIKit

class CategoriesListViewController: UIViewController {

  private let refreshControl: UIRefreshControl = {
    let refreshControl = UIRefreshControl()
    refreshControl.addTarget(self, action: #selector(CategoriesListViewController.refreshTableView(_:)), for: UIControlEvents.valueChanged)
    return refreshControl
  }()

  @IBOutlet private weak var tableView: UITableView? {
    didSet {
      self.tableView?.delegate = self
      self.tableView?.dataSource = self
      self.tableView?.registerNibFrom(CategoryTableViewCell.self)
      if #available(iOS 10.0, *) {
        self.tableView?.refreshControl = self.refreshControl
      } else {
        self.tableView?.addSubview(refreshControl)
      }

      self.tableView?.accessibilityLabel = AccessibilityLabels.categoriesListTableView
    }
  }

  private var dataSource = [Category]() {
    didSet {
      self.tableView?.reloadData()
    }
  }

  private let presenter = CategoriesListPresenter()

  override func viewDidLoad() {
    super.viewDidLoad()
    self.setupViewController()

    self.presenter.attach(view: self)
  }

  func setupViewController() {
    self.title = "Categories"
    self.setupNavigationBar()
  }

  func setupNavigationBar() {
    let navigationBar = self.navigationController?.navigationBar
    navigationBar?.barTintColor = Colors.main
    navigationBar?.tintColor = Colors.title
    navigationBar?.titleTextAttributes = [NSAttributedStringKey.foregroundColor: Colors.title]
  }

  @objc func refreshTableView(_ sender: Any) {
    self.presenter.getAllCategories()
  }

  override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
    if
      segue.identifier == Segues.categoriesListToJoke,
      let destination = segue.destination as? RandomJokeViewController,
      let category = sender as? Category {
      destination.presenter.setCategory(category: category.name)
    }
  }

}

extension CategoriesListViewController: CategoriesListView {

  func showLoading() {
    LoadingHUD.shared?.showLoadingView(at: self.navigationController)
  }

  func hideLoading() {
    LoadingHUD.shared?.hideLoadingView(delay: 1.0)
    self.refreshControl.endRefreshing()
  }

  func updateTable(withDataSource dataSource: [Category]) {
    self.dataSource = dataSource
  }

  func showAlert(withTitle title: String, andMessage message: String) {
    let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.alert)
    let action = UIAlertAction(title: "Ok", style: UIAlertActionStyle.cancel) { (action) in }
    alert.addAction(action)
    self.present(alert, animated: true, completion: nil)
  }

}

extension CategoriesListViewController: UITableViewDelegate {

  func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    let sender = self.dataSource[indexPath.row]
    let identifier = Segues.categoriesListToJoke
    self.performSegue(withIdentifier: identifier, sender: sender)
  }

  func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
    return UIScreen.main.bounds.height / 10
  }
}

extension CategoriesListViewController: UITableViewDataSource {

  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return self.dataSource.count
  }

  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    let cell = tableView.dequeueReusableCell(ofType: CategoryTableViewCell.self) ?? CategoryTableViewCell()
    cell.setup(withText: self.dataSource[indexPath.row].name)
    return cell
  }

}
