//
//  CategoryTableViewCell.swift
//  guiabolso
//
//  Created by Matheus Goveia on 13/09/18.
//  Copyright © 2018 MAOG. All rights reserved.
//

import UIKit

class CategoryTableViewCell: UITableViewCell {

  @IBOutlet private weak var categoryLabel: UILabel? {
    didSet {
      self.categoryLabel?.adjustFontSize()
    }
  }

  override func awakeFromNib() {
    self.accessoryType = UITableViewCellAccessoryType.disclosureIndicator
    self.selectionStyle = UITableViewCellSelectionStyle.none
  }

  func setup(withText text: String) {
    self.categoryLabel?.text = text
    self.accessibilityLabel = text
  }
  
}
