//
//  CategoriesListPresenter.swift
//  guiabolso
//
//  Created by Matheus Goveia on 13/09/18.
//  Copyright © 2018 MAOG. All rights reserved.
//

import Foundation

protocol CategoriesListView: class {
  func updateTable(withDataSource dataSource: [Category])
  func showAlert(withTitle title: String, andMessage message: String)
  func showLoading()
  func hideLoading()
}

class CategoriesListPresenter {

  private weak var view: CategoriesListView? = nil
  
  init() { }

  func attach(view: CategoriesListView?) {
    self.view = view
  }

  func getAllCategories() {
    self.view?.showLoading()
    CategoryService.getAllCategories { (results) in
      self.view?.hideLoading()
      results.handle(
        successful: { (categories) in
          self.view?.updateTable(withDataSource: categories)
      },
        failure: { (error) in
          self.view?.showAlert(withTitle: "Error", andMessage: error.description)
      })
    }
  }

}
