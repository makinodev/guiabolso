//
//  CategoryService.swift
//  guiabolso
//
//  Created by Matheus Goveia on 13/09/18.
//  Copyright © 2018 MAOG. All rights reserved.
//

import Foundation

class CategoryService {
  static func getAllCategories(completion: @escaping (Result<[Category]>) -> ()) {

    let url = "\(URLs.baseURL)/\(URLs.categories)"
    let parameters: JSON? = nil

    APIManager.get(url: url, parameters: parameters) { (results: Result<Any>) in
      results.handle(
        successful: { (result) in
          if let strings = result as? [String] {
            completion(.success(strings.map({ Category(name: $0 )})))
          } else {
            completion(.failure(ParseError.couldNotParse))
          }
      },
        failure: { (error) in
          completion(.failure(error))
      })
    }
  }
}
