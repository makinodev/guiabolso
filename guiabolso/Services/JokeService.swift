//
//  JokeService.swift
//  guiabolso
//
//  Created by Matheus Goveia on 13/09/18.
//  Copyright © 2018 MAOG. All rights reserved.
//

import Foundation

class JokeService {
  static func getRandomJoke(fromCategory category: String, completion: @escaping (Result<Joke>) -> ()) {
    let url = "\(URLs.baseURL)/\(URLs.randomJoke)"
    let parameters = ["category": category]

    APIManager.get(url: url, parameters: parameters) { (results) in
      results.handle(
        successful: { (result) in
          if let result = result as? [String: Any] {
            completion(.success( Joke(withDictionary: result) ))
          } else {
            completion(.failure(ParseError.couldNotParse))
          }
      },
        failure: { (error) in
          completion(.failure(error))
      })
    }
  }
}
