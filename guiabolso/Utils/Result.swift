//
//  Result.swift
//  guiabolso
//
//  Created by Matheus Goveia on 13/09/18.
//  Copyright © 2018 MAOG. All rights reserved.
//

import Foundation

enum Result<T> {
  case success(T)
  case failure(Error)

  var isSuccess: Bool {
    get {
      if case .success(_) = self {
        return true
      } else {
        return false
      }
    }
  }

  var value: T? {
    get {
      if case .success(let v) = self {
        return v
      } else {
        return nil
      }
    }
  }

  var error: Error? {
    get {
      if case .failure(let err) = self {
        return err
      } else {
        return nil
      }
    }
  }

  func handle(successful: ((T) -> Void)? = nil, failure: ((Error) -> Void)? = nil) {
    switch self {
    case .success(let value):
      successful?(value)
    case .failure(let error):
      failure?(error)
    }
  }
}
