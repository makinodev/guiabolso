//
//  AccessibilityLabels.swift
//  guiabolso
//
//  Created by Matheus Goveia on 16/09/18.
//  Copyright © 2018 MAOG. All rights reserved.
//

import Foundation


struct AccessibilityLabels {
  static let categoriesListTableView = "Categories List"
  static let rootView = "Initial Screen"
  static let loadingView = "Loading"
  static let iconImageView = "Chuck Norris"
}
