//
//  Log.swift
//  guiabolso
//
//  Created by Matheus Goveia on 13/09/18.
//  Copyright © 2018 MAOG. All rights reserved.
//

import Foundation

public func log<T>(_ object: T, _ file: String = #file, _ function: String = #function, _ line: Int = #line) {
  #if DEBUG
  let fileString = file as NSString
  let fileLastPathComponent = fileString.lastPathComponent as NSString
  let filename = fileLastPathComponent.deletingPathExtension
  print("\(filename).\(function)[\(line)]: \(object)\n", terminator: "")
  #endif
}
