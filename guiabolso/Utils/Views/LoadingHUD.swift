//
//  LoadingView.swift
//  guiabolso
//
//  Created by Matheus Goveia on 13/09/18.
//  Copyright © 2018 MAOG. All rights reserved.
//

import Foundation
import UIKit

class LoadingHUD {

  // MARK:- Properties

  private(set) static var shared: LoadingHUD? = LoadingHUD()

  private let loadingView = UIView(frame: UIScreen.main.bounds)
  private let grayView = UIView(frame: UIScreen.main.bounds)
  private let imageView = UIImageView(image: #imageLiteral(resourceName: "chucknorris_icon"))

  private init() {
    self.setupGrayView()
    self.setupLoadingView()
    self.setupImageView()
    self.setupRotationObserver()
  }

  @objc func deviceRotated() {
    self.setupLoadingView()
    self.setupGrayView()
    self.setupImageView()
  }

  // MARK:- Animation Methods

  private func fadeIn() {
    self.loadingView.alpha = 0.7
  }

  private func fadeOut() {
    self.loadingView.alpha = 0.0
  }

  private func scaleImageView(to value: CGFloat) {
    self.imageView.transform = CGAffineTransform(scaleX: value, y: value)
  }

  private func setImageViewToIdentity() {
    self.imageView.transform = CGAffineTransform.identity
  }

  private func startViewAnimation(withDuration duration: TimeInterval) {
    UIView.animate(withDuration: duration) {
      self.fadeIn()
    }
  }

  func stopViewAnimation(withDuration duration: TimeInterval) {
    [self.loadingView, self.grayView, self.imageView].forEach({
      $0.layer.removeAllAnimations()
    })
    UIView.animate(withDuration: duration, animations: {
      self.fadeOut()
    }) { (finished) in
      self.reset()
    }
  }

  private func startImageViewAnimation() {
    let duration = 0.35
    UIView.animate(withDuration: duration, animations: {
      self.scaleImageView(to: 0.95)
    }, completion: { (finished) in
      if finished {
        UIView.animate(withDuration: duration, animations: {
          self.setImageViewToIdentity()
        }, completion: { (finished) in
          if finished {
            self.startImageViewAnimation()
          }
        })
      }
    })
  }

  // MARK:- Setup View

  private func setupLoadingView() {
    self.loadingView.frame = UIScreen.main.bounds
    self.loadingView.backgroundColor = UIColor.clear
    self.loadingView.addSubview(self.grayView)
    self.loadingView.alpha = 0.0
    self.loadingView.accessibilityLabel = AccessibilityLabels.loadingView
  }

  private func setupGrayView() {
    self.grayView.frame = UIScreen.main.bounds
    self.grayView.backgroundColor = UIColor.gray
    self.grayView.alpha = 0.7
  }

  private func setupImageView() {
    let frame = UIScreen.main.bounds
    let origin = CGPoint(x: frame.width / 4, y: frame.height / 4)
    let size = CGSize(width: frame.width / 2, height: frame.height / 3)

    self.imageView.contentMode = UIViewContentMode.scaleAspectFit
    self.imageView.frame.origin = origin
    self.imageView.frame.size = size

    self.loadingView.addSubview(self.imageView)

  }

  func setupRotationObserver() {
    NotificationCenter.default.addObserver(self, selector: #selector(LoadingHUD.deviceRotated), name: NSNotification.Name.UIDeviceOrientationDidChange, object: nil)
  }

  func reset() {
    self.imageView.transform = CGAffineTransform.identity
    self.fadeOut()
    self.loadingView.removeFromSuperview()
  }

  // MARK:- Public methods

  func showLoadingView(at viewController: UIViewController?, duration: TimeInterval = 0.5) {
    viewController?.view.addSubview(loadingView)
    self.startViewAnimation(withDuration: duration)
    self.startImageViewAnimation()
  }

  func hideLoadingView(withDuration duration: TimeInterval = 0.5, delay: TimeInterval = 0.0) {
    DispatchQueue.main.asyncAfter(deadline: .now() + delay) {
      self.stopViewAnimation(withDuration: duration)
    }
  }
}
