//
//  Error+Extension.swift
//  guiabolso
//
//  Created by Matheus Goveia on 13/09/18.
//  Copyright © 2018 MAOG. All rights reserved.
//

import Foundation

extension Error {
  var code: Int {
    return (self as NSError).code
  }

  var description: String {
    return "\(String(describing: type(of: self))).\(String(describing: self)) (code \(self.code))"
  }
}
