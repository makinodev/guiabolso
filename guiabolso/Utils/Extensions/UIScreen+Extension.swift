//
//  UIScreen+Extension.swift
//  guiabolso
//
//  Created by Matheus Goveia on 14/09/18.
//  Copyright © 2018 MAOG. All rights reserved.
//

import Foundation
import UIKit

extension UIScreen {

  static func heightSize() -> CGFloat {
    let orientation = UIDevice.current.orientation
    let value: CGFloat
    if orientation == UIDeviceOrientation.landscapeLeft || orientation == UIDeviceOrientation.landscapeRight {
      value = UIScreen.main.bounds.width
    } else {
      value = UIScreen.main.bounds.height
    }
    return value
  }

  static func heightScaleProportion() -> CGFloat {
    return self.heightSize() / 667.0
  }

}
