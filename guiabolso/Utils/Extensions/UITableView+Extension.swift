//
//  UITableView+Extension.swift
//  guiabolso
//
//  Created by Matheus Goveia on 13/09/18.
//  Copyright © 2018 MAOG. All rights reserved.
//

import Foundation
import UIKit

extension UITableView {

  func registerNibFrom(_ cellClass: UITableViewCell.Type) {
    let identifier = String(describing: cellClass)
    let nib = UINib(nibName: identifier, bundle: nil)
    self.register(nib, forCellReuseIdentifier: identifier)
  }

  func dequeueReusableCell<T : UITableViewCell>(ofType type: T.Type) -> T? {
    let identifier = String(describing: type)
    return self.dequeueReusableCell(withIdentifier: identifier) as? T
  }

  func dequeueReusableCell<T : UITableViewCell>(ofType type: T.Type, forIndexPath indexPath: IndexPath) -> T? {
    let identifier = String(describing: type)
    return self.dequeueReusableCell(withIdentifier: identifier, for: indexPath) as? T
  }
}

