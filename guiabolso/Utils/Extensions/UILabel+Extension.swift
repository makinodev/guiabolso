//
//  UILabel+Extension.swift
//  guiabolso
//
//  Created by Matheus Goveia on 14/09/18.
//  Copyright © 2018 MAOG. All rights reserved.
//

import Foundation
import UIKit

extension UILabel {

  func adjustFontSize() {
    let heightProportion = UIScreen.heightScaleProportion()
    if let font = UIFont(name: self.font.fontName, size: heightProportion * self.font.pointSize) {
      self.font = font
    }
  }
}
