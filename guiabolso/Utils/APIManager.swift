//
//  APIManager.swift
//  guiabolso
//
//  Created by Matheus Goveia on 13/09/18.
//  Copyright © 2018 MAOG. All rights reserved.
//

import Foundation
import Alamofire

typealias JSON = [String: Any]

enum GatewayError: Error {
  case parsingError
}

enum NetworkError: Error {
  case unsuccessful(statusCode: Int)
  case noResponse
  case noResult
}

class APIConfigurator {
  fileprivate static var header: HTTPHeaders? = nil
  static func configurateHeader(with header: [String: String]?) {
    self.header = header
  }
}

enum HTTPMethod: String {
  case options = "OPTIONS"
  case get     = "GET"
  case head    = "HEAD"
  case post    = "POST"
  case put     = "PUT"
  case patch   = "PATCH"
  case delete  = "DELETE"
  case trace   = "TRACE"
  case connect = "CONNECT"

  fileprivate func convertAlamo() -> Alamofire.HTTPMethod {
    return Alamofire.HTTPMethod(rawValue: self.rawValue) ?? Alamofire.HTTPMethod.get
  }
}

final class APIManager {

  static func get(url: String, parameters: JSON?, completion: @escaping ((Result<Any>) -> Void)) {
    self.execute(url: url, method: HTTPMethod.get, parameters: parameters, completion: completion)
  }

  private static func execute(url: String, method: HTTPMethod, parameters: JSON?, completion: @escaping ((Result<Any>) -> Void)) {

    Alamofire
      .request(url, method: method.convertAlamo(), parameters: parameters, encoding: JSONEncoding.default, headers: APIConfigurator.header)
      .responseJSON { (dataResponse) in
        if let statusCode = dataResponse.response?.statusCode {
          if 200...299 ~= statusCode {
            if let result = dataResponse.result.value {
              completion(.success(result))
            } else {
              completion(.failure(NetworkError.noResult))
            }
          } else {
            completion(.failure(NetworkError.unsuccessful(statusCode: statusCode)))
          }
        } else {
          completion(.failure(NetworkError.noResponse))
        }
    }
  }
}
