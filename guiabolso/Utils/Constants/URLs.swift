//
//  URLs.swift
//  guiabolso
//
//  Created by Matheus Goveia on 13/09/18.
//  Copyright © 2018 MAOG. All rights reserved.
//

import Foundation

struct URLs {
  private init() { }
  static let baseURL = "https://api.chucknorris.io/jokes"
  static let randomJoke = "random"
  static let categories = "categories"
}
