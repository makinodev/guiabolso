//
//  Colors.swift
//  guiabolso
//
//  Created by Matheus Goveia on 13/09/18.
//  Copyright © 2018 MAOG. All rights reserved.
//

import Foundation
import UIKit

struct Colors {
  static let main: UIColor = #colorLiteral(red: 0.9960784314, green: 0.8274509804, blue: 0.1882352941, alpha: 1)
  static let title: UIColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
  static let text: UIColor = #colorLiteral(red: 0.03921568627, green: 0.03921568627, blue: 0.03921568627, alpha: 1)
}
