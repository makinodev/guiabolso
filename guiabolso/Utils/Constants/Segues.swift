//
//  Segues.swift
//  guiabolso
//
//  Created by Matheus Goveia on 13/09/18.
//  Copyright © 2018 MAOG. All rights reserved.
//

import Foundation

struct Segues {
  static let categoriesListToJoke: String = "categoriesListToJoke"
  static let rootToCategoriesList: String = "rootToCategoriesList"
}
