//
//  RandomJokeViewController.swift
//  guiabolso
//
//  Created by Matheus Goveia on 13/09/18.
//  Copyright © 2018 MAOG. All rights reserved.
//

import Foundation
import UIKit
import Kingfisher
import SafariServices

class RandomJokeViewController: UIViewController {

  let presenter = RandomJokePresenter()

  @IBOutlet private weak var jokeLabel: UILabel? {
    didSet {
      self.jokeLabel?.textAlignment = NSTextAlignment.justified
      self.jokeLabel?.text = String()
      self.jokeLabel?.adjustFontSize()
      self.jokeLabel?.accessibilityLabel = AccessibilityLabels.iconImageView
    }
  }

  @IBOutlet private weak var iconImageView: UIImageView?
  @IBOutlet private weak var linkButton: UIButton? {
    didSet {
      self.linkButton?.setTitle(String(), for: UIControlState.normal)
      self.linkButton?.addTarget(self, action: #selector(RandomJokeViewController.onPressLinkButton(sender:)), for: UIControlEvents.touchUpInside)
      self.linkButton?.titleLabel?.adjustFontSize()
    }
  }

  override func viewDidLoad() {
    self.setupPresenter()
    self.setupViewController()
  }

  func setupViewController() {
    self.title = self.presenter.category
  }

  func setupPresenter() {
    self.presenter.attach(view: self)
    self.presenter.getRandomJoke()
  }

  @objc func onPressLinkButton(sender: UIButton?) {
    if let joke = self.presenter.joke, let url = URL(string: joke.url) {
      let viewController = SFSafariViewController(url: url)
      self.present(viewController, animated: true)
    }
  }
}

extension RandomJokeViewController: RandomJokeView {

  func showLoading() {
    LoadingHUD.shared?.showLoadingView(at: self.navigationController)
  }

  func hideLoading() {
    LoadingHUD.shared?.hideLoadingView()
  }

  func showJoke(joke: Joke) {
    self.jokeLabel?.text = joke.value
    self.iconImageView?.kf.setImage(with: URL(string: joke.icon))
    self.linkButton?.setTitle("Link", for: UIControlState.normal)
  }

  func showAlert(withTitle title: String, andMessage message: String) {
    let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.alert)
    let action = UIAlertAction(title: "Ok", style: UIAlertActionStyle.cancel) { (action) in
      self.navigationController?.popViewController(animated: true)
    }
    alert.addAction(action)
    self.present(alert, animated: true, completion: nil)
  }
}
