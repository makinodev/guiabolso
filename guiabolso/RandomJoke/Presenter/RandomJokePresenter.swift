//
//  RandomJokePresenter.swift
//  guiabolso
//
//  Created by Matheus Goveia on 13/09/18.
//  Copyright © 2018 MAOG. All rights reserved.
//

import Foundation

protocol RandomJokeView: class {
  func showJoke(joke: Joke)
  func showAlert(withTitle title: String, andMessage message: String)
  func showLoading()
  func hideLoading()
}

class RandomJokePresenter {
  
  private weak var view: RandomJokeView? = nil
  private(set) var category = String()
  private(set) var joke: Joke? = nil

  init() { }

  func attach(view: RandomJokeView?) {
    self.view = view
  }

  func setCategory(category: String) {
    self.category = category
  }

  func getRandomJoke() {
    self.view?.showLoading()
    JokeService.getRandomJoke(fromCategory: self.category) { (results) in
      self.view?.hideLoading()
      results.handle(
        successful: { (joke) in
          self.joke = joke
          self.view?.showJoke(joke: joke)
      },
        failure: { (error) in
          self.view?.showAlert(withTitle: "Error", andMessage: error.description)
      })
    }
  }
}
