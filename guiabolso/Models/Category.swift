//
//  Category.swift
//  guiabolso
//
//  Created by Matheus Goveia on 13/09/18.
//  Copyright © 2018 MAOG. All rights reserved.
//

import Foundation

struct Category {
  let name: String

  init(name: String) {
    self.name = name.capitalized
  }
}
