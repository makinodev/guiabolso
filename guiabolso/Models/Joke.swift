//
//  Joke.swift
//  guiabolso
//
//  Created by Matheus Goveia on 13/09/18.
//  Copyright © 2018 MAOG. All rights reserved.
//

import Foundation

struct Joke {
  let icon: String
  let id: String
  let url: String
  let value: String?

  init(withDictionary dictionary: [String: Any]) {
    self.icon = dictionary["icon_url"] as? String ?? ""
    self.id = dictionary["id"] as? String ?? ""
    self.url = dictionary["url"] as? String ?? ""
    self.value = dictionary["value"] as? String ?? ""
  }
}
