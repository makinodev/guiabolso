# guiabolso

## A aplicação contém:
- [x] Uma tela com a lista de categorias;
- [x] Uma tela com o icone, a frase, e o link para a pagina da piada da categoria escolhida;

- [x] Versão minima do iOS: 9.*;
- [x] Escrito em Swift.

### Bônus:
- [x] Recomendada a utilização do Cocoapods;
- [x] Testes Unitário;
- [x] Testes Funcionais;
- [x] App Universal;
- [x] cache das imagens;
- [x] Testes Unitário;
- [ ] arquitetura MVVM;
- [ ] Utilização de RxSwift.

## Frameworks utilizadas:
* Alamofire;
* Kingfisher;
* Nimble;
* KIF.

## O link do repositório público
https://gitlab.com/makinodev/guiabolso


## Instruções de compilação
```
git clone git@gitlab.com:makinodev/guiabolso.git
cd guiabolso
open guiabolso.xcworkspace
⌘ + R
```

## Principais decisões técnicas
* MVP: O projeto *tem* que ter testes unitários/funcionais e com MVP fica fácil de separar as partes (lógica/interface) e fazer os testes de cada uma. Tenho costume de fazer projetos assim.
* Organizei para que o código de conexão (`import Alamofire`) exista apenas num arquivo. Caso precise fazer uma troca do *framework* seria necessário mudar apenas em um lugar. No caso "APIManager.swift". O mesmo aconteceria caso existisse um banco de dados. Para fazer o uso desses serviços, utilizo *services* com o nome da *model* correspondente que já entregam os valores mastigados para as *presenters*.
* Tenho costume de colocar constantes em um lugar apenas (Ex: *segues*, cores, *url*, *labels* de acessibilidade). Se precisar mudar o valor de uma então o código mudaria automaticamente em todos os outros lugares. Fica mais fácil e rápido de editar.
* Geralmente aproveito, em variáveis *IBOulet*, seu *didSet* para customizá-las. Tenho preferência para utilizar esse pequeno espaço de código para evitar conflitos de merge em *xib* ou *storyboard*. Deixo apenas uma *View Controller* por *Storyboard* pelo mesmo motivo.
* O cache de imagens foi feito utilizando *Kingfisher*. Testes foram feitos utilizando o *Nimble* e *KIF*. E a conexão a API através do *Alamofire*

**Batata**
